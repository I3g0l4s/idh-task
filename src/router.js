import Vue from 'vue'
import Router from 'vue-router'
import Team from './pages/Team.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'team',
      component: Team
    }
  ]
})
