import Vue from 'vue'
import Vuex from 'vuex'
import * as team from './team/index'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    team
  }
})
