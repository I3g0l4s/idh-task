export class EmployeeModel {
  constructor (id,
    title,
    position,
    place,
    thumbnail,
    contact) {
    this.id = id
    this.thumbnail = `photo_${this.id}`
    this.contact = contact || 'test@gmail.com'
    this.title = title
    this.position = position
    this.place = place
  }
}
