import {EmployeeModel} from '@/store/team/models/employee.model'

export const state = {
  employees: [
    new EmployeeModel(
      1,
      'Basia Sołtysińska',
      'Founder',
      'Warsaw'
    ),
    new EmployeeModel(
      2,
      'Włas Chorowiec',
      'Founder',
      'Warsaw'
    ),
    new EmployeeModel(
      3,
      'Edyta Leśniewska',
      'Head of Human Resources',
      'Warsaw'
    ),
    new EmployeeModel(
      4,
      'Joanna Pawluk',
      'Chief Growth Officer (CGO)',
      'London'
    ),
    new EmployeeModel(
      5,
      'David Saunders',
      'Business Development',
      'London'
    ),
    new EmployeeModel(
      6,
      'Adam Eldridge',
      'Business Development Director US',
      'Los Angeles'
    ),
    new EmployeeModel(
      7,
      'Maha Mahda',
      'Chief Business  Development Officer (CBDO)',
      'Warsaw'
    ),
    new EmployeeModel(
      8,
      'Elżbieta Kamińska',
      'International Client Service Manager',
      'Warsaw'
    ),
    new EmployeeModel(
      9,
      'Karolina Makuch',
      'Creation Manager',
      'Warsaw'
    ),
    new EmployeeModel(
      10,
      'Kamil Bolek',
      'Head of Communication',
      'Warsaw'
    )
  ]
}

export const profile = {
  state
}

export const namespaced = true
